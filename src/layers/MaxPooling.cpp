#include "MaxPooling.h"

#include <iostream>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"

namespace ML {
// --- Begin Student Code ---

// Compute the convultion for the layer data
void MaxPoolingLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...

    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    const LayerParams& outParam = output.getParams();

    // std::cout << "\nSIZES\n";
    // std::cout << "Input dims size: " << inParam.dims.size() << '\n';
    // std::cout << "Output dims size: " << outParam.dims.size() << '\n';

    // std::cout << "\nDIMENSIONS\n";
    // std::cout << "Input dims: " << inParam.dims[0] << 'x' << inParam.dims[1] << 'x' << inParam.dims[2] << '\n';
    // std::cout << "Output dims: " << outParam.dims[0] << 'x' << outParam.dims[1] << 'x' << outParam.dims[2] << '\n';
    // std::cout << "\n";
    
    int stride = 2;
    int iC = (int)inParam.dims[2]; // input channels = output channels
    // int iH = (int)inParam.dims[0]; // input height
    int iW = (int)inParam.dims[1]; // input width
    int oP = (int)outParam.dims[0]; // output height
    int oQ = (int)outParam.dims[1]; // output width

    // 1-D indexing: rowInd*width+colInd
    for (int c = 0; c < iC; c++) {
        for (int p = 0; p < oP; p++) {
            for (int q = 0; q < oQ; q++) {

                int oInd = p*(oQ*iC)+q*iC+c;
                fp32 max = 0;

                for (int h = stride*p; h < stride*(p+1); h++) {
                    for (int w = stride*q; w < stride*(q+1); w++) {
                        int iInd = h*(iW*iC)+w*iC+c;
                        fp32 temp = dataIn.get<fp32>(iInd);
                        max =  temp > max ? temp : max;
                    }
                }
            
                output.get<fp32>(oInd) = max;
            }
        }
    }
}

void MaxPoolingLayer::computeAccelerated(const LayerData& dataIn) const {
    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    const LayerParams& outParam = output.getParams();
    
    int stride = 2;
    int iC = (int)inParam.dims[2]; // input channels = output channels
    // int iH = (int)inParam.dims[0]; // input height
    int iW = (int)inParam.dims[1]; // input width
    int oP = (int)outParam.dims[0]; // output height
    int oQ = (int)outParam.dims[1]; // output width

    // 1-D indexing: rowInd*width+colInd
    for (int c = 0; c < iC; c++) {
        for (int p = 0; p < oP; p++) {
            for (int q = 0; q < oQ; q++) {

                int oInd = p*(oQ*iC)+q*iC+c;
                fp32 max = 0;

                for (int h = stride*p; h < stride*(p+1); h++) {
                    for (int w = stride*q; w < stride*(q+1); w++) {
                        int iInd = h*(iW*iC)+w*iC+c;
                        fp32 temp = dataIn.get<fp32>(iInd);
                        max =  temp > max ? temp : max;
                    }
                }
            
                output.get<fp32>(oInd) = max;
            }
        }
    }
}

// Compute the convolution using threads
void MaxPoolingLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void MaxPoolingLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void MaxPoolingLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML
