#include "Flatten.h"

#include <iostream>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"

namespace ML {
// --- Begin Student Code ---

// Compute the convultion for the layer data
void FlattenLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...

    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    // const LayerParams& outParam = output.getParams();

    // std::cout << "\nSIZES\n";
    // std::cout << "Input dims size: " << inParam.dims.size() << '\n';
    // std::cout << "Output dims size: " << outParam.dims.size() << '\n';

    // std::cout << "\nDIMENSIONS\n";
    // std::cout << "Input dims: " << inParam.dims[0] << 'x' << inParam.dims[1] << 'x' << inParam.dims[2] << '\n';
    // std::cout << "Output dims: " << outParam.dims[0] << 'x' << outParam.dims[1] << 'x' << outParam.dims[2] << '\n';
    // std::cout << "\n";

    int iC = (int)inParam.dims[2]; // input channels = output channels
    int iH = (int)inParam.dims[0]; // input height
    int iW = (int)inParam.dims[1]; // input width

    // 1-D indexing: rowInd*width+colInd
    for (int c = 0; c < iC; c++) {
        for (int h = 0; h < iH; h++) {
            for (int w = 0; w < iW; w++) {

                int ind = h*(iW*iC)+w*iC+c;
            
                output.get<fp32>(ind) = dataIn.get<fp32>(ind);
            }
        }
    }
}

void FlattenLayer::computeAccelerated(const LayerData& dataIn) const {
    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();

    int iC = (int)inParam.dims[2]; // input channels = output channels
    int iH = (int)inParam.dims[0]; // input height
    int iW = (int)inParam.dims[1]; // input width

    // 1-D indexing: rowInd*width+colInd
    for (int c = 0; c < iC; c++) {
        for (int h = 0; h < iH; h++) {
            for (int w = 0; w < iW; w++) {

                int ind = h*(iW*iC)+w*iC+c;
            
                output.get<fp32>(ind) = dataIn.get<fp32>(ind);
            }
        }
    }
}

// Compute the convolution using threads
void FlattenLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void FlattenLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void FlattenLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML
