#include "Dense.h"

#include <iostream>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"
#ifdef ZEDBOARD
#include "xil_io.h"
#include "xparameters.h"
#include "xllfifo_hw.h"
#endif

namespace ML {
// --- Begin Student Code ---
int8_t DenseLayer::quantizeValue(fp32 value, bool isActivation) const { 
    return isActivation ? (zero_points + round(value * scaleValueInputs)) : round(value * scaleValueWeights) ;
}
// Compute the convultion for the layer data
void DenseLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...

    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    const LayerParams& outParam = output.getParams();

    
    //int stride = 1;
    //int iC = (int)inParam.dims[2]; // input channels
    int iH = (int)inParam.dims[0]; // input height
    //int iW = (int)inParam.dims[1]; // input width
    //int fR = (int)weightParam.dims[0]; // filter height
    //int fS = (int)weightParam.dims[1]; // filter width
    int oM = (int)outParam.dims[0]; // output channels
    //int oP = (iH-fR+stride)/stride; // output height, same as outParam.dims[0]
    //int oQ = (iW-fS+stride)/stride; // output width, same as outParam.dims[1]

    int8_t* quantValuesptr = (int8_t*)malloc(inParam.flat_count());
    for(int i =0; i< (int)inParam.flat_count();i++){
        *(quantValuesptr +i) = quantizeValue(dataIn.get<fp32>(i),true);
       
    }
    // averageI = averageI/(int)inParam.flat_count();
    // fp32 maxWeight=0.0;
    // int8_t* quantWeightptr = (int8_t*)malloc(weightParam.flat_count());

    // for(int i =0; i< (int)weightParam.flat_count();i++){
    //     *(quantWeightptr +i) = quantizeValue(weightData.get<fp32>(i),false);

    //         //  std::ofstream wf("dense_8bit_weight_1.bin", std::ios::app | std::ios::binary);

    //         //     if (!wf) {
    //         //         std::cout << "Cannot open file!" << std::endl;
                    
    //         //     }
    //         //     int8_t value =  *(quantWeightptr +i);
    //         //     wf.write((char*)&value,sizeof(value));
    //         //     wf.close();


    // }

    fp32 sumOfWeights=0.0;

    //for each output
    for (int o=0; o<oM; o++){

        fp32 sum = 0;

        //fp32 sum2 = 0;
        sumOfWeights = 0;
        for(int i=0; i<iH; i++){
            int iInd = i;
            int fInd = o+ (i * oM);
            //sum2 += dataIn.get<fp32>(iInd) * weightData.get<fp32>(fInd);
            int8_t weightValue = weightData.get<int8_t>(fInd);
            int8_t activationValue =  *(quantValuesptr +iInd);
            sumOfWeights +=weightValue;
            sum += activationValue * weightValue;
        }
        //   std::ofstream wf("dense_8bit_sumWeight_1.bin", std::ios::app | std::ios::binary);

        //         if (!wf) {
        //             std::cout << "Cannot open file!" << std::endl;
                    
        //         }
        //         fp32 value =  sumOfWeights;
        //         wf.write((char*)&value,sizeof(value));
        //         wf.close();
        //sum2 += biasData.get<fp32>(o);
        
        //int32_t biasValue = round(scaleValueInputs * scaleValueWeights * biasData.get<fp32>(o));
        sum += biasData.get<fp32>(o);

        //  std::cout <<"Start";
        //         std::cout <<"\n";
        //         std::cout << sumOfWeights;
        //         std::cout <<"\n";
        //         std::cout << sumWeightData.get<fp32>(o);
        //         std::cout <<"\n";
        fp32 outputValue = (sum-  (zero_points* sumWeightData.get<fp32>(o)))/(scaleValueInputs * scaleValueWeights) ;
        //output.get<fp32>(o) = iH == 256 ? sum2 : sum2 < 0 ? 0 : sum2; // hard code no reLU for dense 2
        output.get<fp32>(o) = iH == 256 ? outputValue : outputValue < 0 ? 0 : outputValue;
        // output.get<fp32>(o) = sum < 0 ? 0 : sum;

        //  std::ofstream wf("dense_8bit_bias_1.bin", std::ios::app | std::ios::binary);

        //         if (!wf) {
        //             std::cout << "Cannot open file!" << std::endl;
                    
        //         }
        //         fp32 value =  biasValue;
        //         wf.write((char*)&value,sizeof(value));
        //         wf.close();


         
    }

    //free(quantWeightptr);
    free(quantValuesptr);
}

void DenseLayer::computeAccelerated(const LayerData& dataIn) const {
 #ifdef ZEDBOARD
    // TODO: Your Code Here...

    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    const LayerParams& outParam = output.getParams();
    int iH = (int)inParam.dims[0]; // input height
    int oM = (int)outParam.dims[0]; // output channels

    int8_t* quantValuesptr = (int8_t*)malloc(inParam.flat_count());
    for(int i =0; i< (int)inParam.flat_count();i++){
        *(quantValuesptr +i) = quantizeValue(dataIn.get<fp32>(i),true);
    }
    // fp32 maxWeight=0.0;
    // int8_t* quantWeightptr = (int8_t*)malloc(weightParam.flat_count());

    // for(int i =0; i< (int)weightParam.flat_count();i++){
    //     *(quantWeightptr +i) = quantizeValue(weightData.get<fp32>(i),false);
    // }

    //fp32 sumOfWeights=0.0;

    //for each output
    for (int o=0; o<oM; o++){
        //sumOfWeights = 0;
        //logInfo(std::to_string(o));
        for(int i=0; i<iH; i++){
            int iInd = i;
            int fInd = o+ (i * oM);
            //sum2 += dataIn.get<fp32>(iInd) * weightData.get<fp32>(fInd);
            int8_t weightValue = weightData.get<int8_t>(fInd);
            int8_t activationValue =  *(quantValuesptr +iInd);
            //sumOfWeights +=weightValue;
            //sum += activationValue * weightValue;

            uint16_t DataValue= ((uint8_t)weightValue << 8) | (uint8_t)activationValue;
                            //std::cout << std::hex <<(int)DataValue;
                            //std::cout <<"\n";
            Xil_Out32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_TDFD_OFFSET, (uint32_t)DataValue);
        }
        Xil_Out32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_TLF_OFFSET, iH*sizeof(u32));
        //sum2 += biasData.get<fp32>(o);
        

        while (Xil_In32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_RDFO_OFFSET) == 0) { }
         while (true) {
                        //logInfo("--- In Forever Loop ---");
                        // Then read how many words are available to us right now.
                        // Bit 31 = 0 when this is all the words in the current packet
                        // Bit 31 = 1 when this is how many words are available,
                        // but no TLAST has been sent to us yet
                        uint32_t read_len = Xil_In32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_RLF_OFFSET);
                        //logInfo(std::to_string(read_len));
                        // Read out every word we have access to right now
                        for (long unsigned int i = 0; i < (read_len & 0x7FFFFFFFUL); i+=4) {
                            int32_t in_data = Xil_In32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_RDFD_OFFSET);
                            // logInfo("Mac Output");
                            // logInfo(std::to_string(in_data));
                            // logInfo("SHould be output");
                            // logInfo(std::to_string(sum));
                            // Process in_data here

                            //Set the output
                            //int32_t biasValue = round(scaleValueInputs * scaleValueWeights * biasData.get<fp32>(o));
                            in_data += biasData.get<fp32>(o);
                            //sum += biasValue;
                            //logInfo(std::to_string(sum));
                            //convert in_data to fp32
                            fp32 outputValue = ((in_data * 1.0) -  (zero_points* sumWeightData.get<fp32>(o)))/(scaleValueInputs * scaleValueWeights); 
                            // Process in_data here, note that it is really a uint8_t, the upper 24 bits will always b
                           
                            output.get<fp32>(o) = iH == 256 ? outputValue : outputValue < 0 ? 0 : outputValue;
                            //logInfo(std::to_string(outputValue));  iH == 256 ? outputValue : outputValue < 0 ? 0 : outputValue
                        }
                        //logInfo("Here");

                        if (~read_len & (1 << 31)) {
                            //logInfo("--- Exit Loop ---");
                            //HUH
                            break; // This is all the data in this packet, done
                        }
                // There is more in this data packet, wait for more to come in
                }
         
    }

    //free(quantWeightptr);
    free(quantValuesptr);
 #endif
}

// Compute the convolution using threads
void DenseLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void DenseLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void DenseLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML
