#include "Softmax.h"

#include <iostream>
#include <cmath>

#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"

namespace ML {
// --- Begin Student Code ---

// Compute the convultion for the layer data
void SoftmaxLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...

    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    // const LayerParams& outParam = output.getParams();

    // std::cout << "\nSIZES\n";
    // std::cout << "Input dims size: " << inParam.dims.size() << '\n';
    // std::cout << "Output dims size: " << outParam.dims.size() << '\n';

    // std::cout << "\nDIMENSIONS\n";
    // std::cout << "Input dims: " << inParam.dims[0] << '\n';
    // std::cout << "Output dims: " << outParam.dims[0] << '\n';
    // std::cout << "\n";
    
    int N = (int)inParam.dims[0];

    fp32 sum = 0;

    for (int l = 0; l < N; l++) {
        sum += exp(dataIn.get<fp32>(l));
    }

    for (int i = 0; i < N; i++) {
        output.get<fp32>(i) = exp(dataIn.get<fp32>(i))/sum;
    }
    
}

void SoftmaxLayer::computeAccelerated(const LayerData& dataIn) const {
    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    
    int N = (int)inParam.dims[0];

    fp32 sum = 0;

    for (int l = 0; l < N; l++) {
        sum += exp(dataIn.get<fp32>(l));
    }

    for (int i = 0; i < N; i++) {
        output.get<fp32>(i) = exp(dataIn.get<fp32>(i))/sum;
    }
}

// Compute the convolution using threads
void SoftmaxLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void SoftmaxLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void SoftmaxLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}
}  // namespace ML
