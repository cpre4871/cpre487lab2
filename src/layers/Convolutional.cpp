#include "Convolutional.h"

#include <iostream>
#include <cstdlib>
#include "../Types.h"
#include "../Utils.h"
#include "Layer.h"
#include <vector>
#ifdef ZEDBOARD
#include "xil_io.h"
#include "xparameters.h"
#include "xllfifo_hw.h"
#endif

#include<fstream>
namespace ML {
// --- Begin Student Code ---
int8_t ConvolutionalLayer::quantizeValue(fp32 value, bool isActivation) const { 
    return isActivation ? (zero_points + round(value * scaleValueInputs)) : round(value * scaleValueWeights) ;
}
// Compute the convultion for the layer data
void ConvolutionalLayer::computeNaive(const LayerData& dataIn) const {
    // TODO: Your Code Here...
    
    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    const LayerParams& outParam = output.getParams();

    //LayerData& sumOfWeightinfo = getsumOfWeightData();
    
    //std::cout << sumOfWeightData.get<fp32>(0);
    //std::vector<int8_t> vec_buf;
    // std::cout << "\nSIZES\n";
    // std::cout << "Input dims size: " << inParam.dims.size() << '\n';
    // std::cout << "Weight dims size: " << weightParam.dims.size() << '\n';
    // std::cout << "Bias dims size: " << biasParam.dims.size() << '\n';
    // std::cout << "Output dims size: " << outParam.dims.size() << '\n';

    // std::cout << "\nDIMENSIONS\n";
    // std::cout << "Input dims: " << inParam.dims[0] << 'x' << inParam.dims[1] << 'x' << inParam.dims[2] << '\n';
    // std::cout << "Weight dims: " << weightParam.dims[0] << 'x' << weightParam.dims[1] << 'x' << weightParam.dims[2] << 'x' << weightParam.dims[3] << '\n';
    // std::cout << "Bias dims: " << biasParam.dims[0] << '\n';
    // std::cout << "Output dims: " << outParam.dims[0] << 'x' << outParam.dims[1] << 'x' << outParam.dims[2] << '\n';
    // std::cout << "\n";
    //int8_t* quantValues [(int)inParam.flat_count()];
    //int8_t* quantValuesptr = (int8_t*)malloc(inParam.flat_count());
    //fp32 averageI = 0.0;
    //fp32 maxI = 0.0;
    int8_t quantizeValueAct [115200];
    for(int i =0; i< (int)inParam.flat_count();i++){
        //*(quantValuesptr +i) = quantizeValue(dataIn.get<fp32>(i),true);
        quantizeValueAct[i]=quantizeValue(dataIn.get<fp32>(i),true);
        // averageI +=dataIn.get<fp32>(i);
        // maxI =  std::max(dataIn.get<fp32>(i),maxI);
    }
    //averageI = averageI/(int)inParam.flat_count();
    //fp32 maxWeight=0.0;
    //int8_t* quantWeightptr = (int8_t*)malloc(weightParam.flat_count());

    // for(int i =0; i< (int)weightParam.flat_count();i++){
    //     *(quantWeightptr +i) = quantizeValue(weightData.get<fp32>(i),false);
    // }
        // std::ofstream wf("weightQuant8bit_layer8.bin", std::ios::app | std::ios::binary);

        // if (!wf) {
        //     std::cout << "Cannot open file!" << std::endl;
            
        // }
        // int8_t value =  *(quantWeightptr +i);
        // wf.write((char*)&value,sizeof(value));
        // wf.close();
    // }

	// std::vector<uint32_t> vec_buf;
    //     std::ifstream bin_file("sumOf_weights_8bit_layer0.bin", std::ios::binary);

    //     if (bin_file.good()) {
    //         /*Read Binary data using streambuffer iterators.*/
    //         std::vector<char32_t> v_buf((std::istreambuf_iterator<char32_t>(bin_file)), (std::istreambuf_iterator<char32_t>()));
    //         vec_buf = v_buf;
    //         bin_file.close();
    //     }

    //     else {
    //         throw std::exception();
    //     }

    //int8_t* quantWeightptr = (int8_t*)malloc(weightParam.flat_count());

    // for(int i =0; i< (int)weightParam.flat_count();i++){
    //     *(quantWeightptr +i) =weightData.get<int8_t>(i);
    // }
    fp32 sumOfWeights=0.0;

    //std::cout << inParam.flat_count();
    int stride = 1;
    int iC = (int)inParam.dims[2]; // input channels
    int iH = (int)inParam.dims[0]; // input height
    int iW = (int)inParam.dims[1]; // input width
    int fR = (int)weightParam.dims[0]; // filter height
    int fS = (int)weightParam.dims[1]; // filter width
    int oM = (int)outParam.dims[2]; // output channels
    int oP = (iH-fR+stride)/stride; // output height, same as outParam.dims[0]
    int oQ = (iW-fS+stride)/stride; // output width, same as outParam.dims[1]
    
    // 1-D indexing: rowInd*width+colInd
    for (int m = 0; m < oM; m++) {
        //int32_t biasValue = 0;
        for (int p = 0; p < oP; p++) {
            for (int q = 0; q < oQ; q++) {

                int oInd = p*(oQ*oM)+q*oM+m;
                fp32 sum = 0;
                fp32 sum2 = 0;
                sumOfWeights = 0;
                for (int c = 0; c < iC; c++) {
                    for (int r = 0; r < fR; r++) {
                        for (int s = 0; s < fS; s++) {
                            int iInd = (stride*p+r)*(iW*iC)+(stride*q+s)*iC+c;
                            int fInd = r*(fS*iC*oM)+s*(iC*oM)+c*oM+m;
                            
                            sum2 += dataIn.get<fp32>(iInd) * weightData.get<fp32>(fInd);
                            int8_t activationValue =  quantizeValueAct[iInd];
                            //int8_t activationValue =  quantizeValue(dataIn.get<fp32>(iInd),true);
                            //*(quantWeightptr +i)
                            //int8_t weightValue = *(quantWeightptr +fInd);
                            int8_t weightValue = weightData.get<int8_t>(fInd);
                            //int8_t weightValue = vec_buf[fInd];
                            //int8_t weightValue = quantizeValue(weightData.get<fp32>(fInd),false);
                            //std::cout <<"Start";
                            //std::cout <<"\n";
                            //std::cout << std::hex<<(uint32_t)activationValue;
                            //std::cout <<"\n";
                            //std::cout << (uint32_t)weightValue;
                            //std::cout <<"\n";
                            //uint16_t DataValue= ((uint8_t)weightValue << 8) | (uint8_t)activationValue;
                            //std::cout << std::hex <<(int)DataValue;
                            //std::cout <<"\n";

                            //send data
                            sumOfWeights +=weightValue;
                            sum += activationValue * weightValue;
                            
                        }
                    }
                }
                //sum2 += biasData.get<fp32>(m);

                //biasValue = round(scaleValueInputs * scaleValueWeights * biasData.get<fp32>(m));
                sum += biasData.get<fp32>(m);
                fp32 outputValue = (sum-  (zero_points* sumOfWeightData.get<fp32>(m)))/(scaleValueInputs * scaleValueWeights) ;
                // std::cout <<"Start";
                // std::cout <<"\n";
                // std::cout << sumOfWeights;
                // std::cout <<"\n";
                // std::cout << sumOfWeightData.get<fp32>(m);
                // std::cout <<"\n";
                
                output.get<fp32>(oInd) = outputValue < 0 ? 0 : outputValue;
                //output.get<fp32>(oInd) = outputValue;
            }
        }

                // std::ofstream wf("bias_8bit_layer8.bin", std::ios::app | std::ios::binary);

                // if (!wf) {
                //     std::cout << "Cannot open file!" << std::endl;
                    
                // }
                // fp32 value =  biasValue;
                // wf.write((char*)&value,sizeof(value));
                // wf.close();
    }

    //free(quantWeightptr);
    //free(quantValuesptr);
    
}

void ConvolutionalLayer::computeAccelerated(const LayerData& dataIn) const {
     //logInfo("--- JHere4---");
#ifdef ZEDBOARD
 //logInfo("--- JHere5 ---");
    const LayerParams& inParam = dataIn.getParams();

    LayerData& output = getOutputData();
    const LayerParams& outParam = output.getParams();

    //std::vector<uint8_t> vec_buf;
    int8_t* quantValuesptr = (int8_t*)malloc(inParam.flat_count());
    for(int i =0; i< (int)inParam.flat_count();i++){
        *(quantValuesptr +i) = quantizeValue(dataIn.get<fp32>(i),true);
    }
    //int8_t* quantWeightptr = (int8_t*)malloc(weightParam.flat_count());

    // for(int i =0; i< (int)weightParam.flat_count();i++){
    //     *(quantWeightptr +i) = quantizeValue(weightData.get<fp32>(i),false);
    // }

    //int8_t* quantWeightptr = (int8_t*)malloc(weightParam.flat_count());

    // for(int i =0; i< (int)weightParam.flat_count();i++){
    //     *(quantWeightptr +i) = weightData.get<int8_t>(i);
    // }


    // std::ifstream bin_file(file_name, std::ios::binary);

    //     if (bin_file.good()) {
    //         /*Read Binary data using streambuffer iterators.*/
    //         std::vector<uint8_t> v_buf((std::istreambuf_iterator<char>(bin_file)), (std::istreambuf_iterator<char>()));
    //         vec_buf = v_buf;
    //         bin_file.close();
    //     }
    //fp32 sumOfWeights=0.0;

    //std::cout << inParam.flat_count();
    int stride = 1;
    int iC = (int)inParam.dims[2]; // input channels
    int iH = (int)inParam.dims[0]; // input height
    int iW = (int)inParam.dims[1]; // input width
    int fR = (int)weightParam.dims[0]; // filter height
    int fS = (int)weightParam.dims[1]; // filter width
    int oM = (int)outParam.dims[2]; // output channels
    int oP = (iH-fR+stride)/stride; // output height, same as outParam.dims[0]
    int oQ = (iW-fS+stride)/stride; // output width, same as outParam.dims[1]
    //logInfo(std::to_string(fR));
    // 1-D indexing: rowInd*width+colInd

    for (int p = 0; p < oP; p++) {
        for (int q = 0; q < oQ; q++) {
             for (int m = 0; m < oM; m++) {
                //int32_t sum =0;
                //sumOfWeights = 0;
               
                for (int r = 0; r < fR; r++) {
                    for (int s = 0; s < fS; s++) {
                        for (int c = 0; c < iC; c++) {
                            int iInd = (stride*p+r)*(iW*iC)+(stride*q+s)*iC+c;
                            int fInd = r*(fS*iC*oM)+s*(iC*oM)+c*oM+m;
                            //logInfo(std::to_string(r));
                            //sum2 += dataIn.get<fp32>(iInd) * weightData.get<fp32>(fInd);
                            int8_t activationValue =  *(quantValuesptr +iInd);
                            //int8_t activationValue =  quantizeValue(dataIn.get<fp32>(iInd),true);
                            //*(quantWeightptr +i)
                            //int8_t weightValue = *(quantWeightptr +fInd);
                            int8_t weightValue = weightData.get<int8_t>(fInd);
                            //sumOfWeights +=weightValue;
                            //int8_t weightValue = quantizeValue(weightData.get<fp32>(fInd),false);
                            //std::cout <<"Start";
                            //std::cout <<"\n";
                            //std::cout << std::hex<<(uint32_t)activationValue;
                            //std::cout <<"\n";
                            //std::cout << (uint32_t)weightValue;
                            //std::cout <<"\n";
                            uint16_t DataValue= ((uint8_t)weightValue << 8) | (uint8_t)activationValue;
                            //std::cout << std::hex <<(int)DataValue;
                            //std::cout <<"\n";
                            Xil_Out32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_TDFD_OFFSET, (uint32_t)DataValue);
                            //logInfo(std::to_string(DataValue));
                            //if((c == iC-1) and (r == fR-1) and (s == fS-1)){
                                     //logInfo("--- JHere1 ---");
                            //Xil_Out32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_TDFD_OFFSET, DataValue);
   
                            
                            //send data
                            //sumOfWeights +=weightValue;
                            //sum += activationValue * weightValue;
                            
                        }
                    }
                }
                Xil_Out32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_TLF_OFFSET, iC*fR*fS*sizeof(u32));

                //logInfo("--- HERe17 ---");
                //int timeout =1000;
                    while (Xil_In32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_RDFO_OFFSET) == 0) {
                        //logInfo("--- HERE I GUESS ---");
                        //timeout--;
                    }
                    //logInfo("--- HERe19 ---");
                    while (true) {
                        int oInd = p*(oQ*oM)+q*oM+m;
                        //logInfo("--- In Forever Loop ---");
                        // Then read how many words are available to us right now.
                        // Bit 31 = 0 when this is all the words in the current packet
                        // Bit 31 = 1 when this is how many words are available,
                        // but no TLAST has been sent to us yet
                        uint32_t read_len = Xil_In32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_RLF_OFFSET);
                        //logInfo(std::to_string(read_len));
                        // Read out every word we have access to right now
                        for (long unsigned int i = 0; i < (read_len & 0x7FFFFFFFUL); i+=4) {
                            int32_t in_data = Xil_In32(XPAR_AXI_FIFO_0_BASEADDR + XLLF_RDFD_OFFSET);
                            // logInfo("Mac Output");
                            // logInfo(std::to_string(in_data));
                            // logInfo("SHould be output");
                            // logInfo(std::to_string(sum));
                            // Process in_data here

                            //Set the output
                            //int32_t biasValue = round(scaleValueInputs * scaleValueWeights * biasData.get<fp32>(m));
                            in_data += biasData.get<fp32>(m);
                            //sum += biasValue;
                            //logInfo(std::to_string(sum));
                            //convert in_data to fp32
                            fp32 outputValue = ((in_data * 1.0) -  (zero_points* sumOfWeightData.get<fp32>(m)))/(scaleValueInputs * scaleValueWeights); 
                            // Process in_data here, note that it is really a uint8_t, the upper 24 bits will always b
                            oInd = p*(oQ*oM)+q*oM+m;
                            output.get<fp32>(oInd) = outputValue < 0 ? 0 : outputValue;
                            //logInfo(std::to_string(outputValue));
                        }
                        //logInfo("Here");

                        if (~read_len & (1 << 31)) {
                            //logInfo("--- Exit Loop ---");
                            //HUH
                            break; // This is all the data in this packet, done
                        }
                // There is more in this data packet, wait for more to come in
                }
                //sum2 += biasData.get<fp32>(m);
                //int32_t biasValue = round(scaleValueInputs * scaleValueWeights * biasData.get<fp32>(m));
                //sum += biasValue;
                //fp32 outputValue = (sum-  (zero_points* sumOfWeights))/(scaleValueInputs * scaleValueWeights) ;

                
                //output.get<fp32>(oInd) = outputValue < 0 ? 0 : outputValue;
                //output.get<fp32>(oInd) = outputValue;
            }
        }
        //logInfo(std::to_string(m));
    }

    //free(quantWeightptr);
    free(quantValuesptr);
#endif
}

// Compute the convolution using threads
void ConvolutionalLayer::computeThreaded(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using a tiled approach
void ConvolutionalLayer::computeTiled(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}

// Compute the convolution using SIMD
void ConvolutionalLayer::computeSIMD(const LayerData& dataIn) const {
    // TODO: Your Code Here...
}


}  // namespace ML
